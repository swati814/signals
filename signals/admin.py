from django.contrib import admin
from . import models


admin.site.register(models.UserProfile)
admin.site.register(models.User)
admin.site.register(models.Relation)
# admin.site.register(models.ProfileDelete)
