from django.db import models
from django.db.models.signals import post_save, pre_save
from django.contrib.postgres.fields import ArrayField
from django.contrib.auth.models import AbstractBaseUser
from django.dispatch import receiver
# from .manager import CustomManager


class User(AbstractBaseUser):
    TYPE = (
        ('manager', 'Manager'),
        ('employee', 'Employee'),
    )
    email = models.EmailField(unique=True, null=True)
    username = models.CharField(max_length=22, unique=True)
    first_name = models.CharField(max_length=30, null=True)
    last_name = models.CharField(max_length=30, null=True)
    type = models.CharField(max_length=50, choices=TYPE, blank=False, null=False)

    USERNAME_FIELD = "username"
    REQUIRED_FIELDS = []

    def __str__(self):
        return str(self.username)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    country = models.CharField(max_length=100, default='India')
    city = models.CharField(max_length=100, default='Blr')

    @receiver(post_save, sender=User)
    def act(sender, instance, created, **kwargs):
        if created:
            UserProfile.objects.create(user=instance)


class Relation(models.Model):
    userob = models.OneToOneField(User, null=True, related_name='users')
    mgr = models.ForeignKey(User, null=True, related_name='manager')

    @receiver(post_save, sender=User)
    def rel(sender, instance, created, **kwargs):
        if created:
            uob = User.objects.get(username="manager007")
            rm = Relation.objects.get(mgr=uob)
            rm.userob = instance
            rm.save()
            # Relation.objects.create(userob=instance, mgr=rm)